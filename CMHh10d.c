/*
 * CMHh10d.c
 *
 * Created: 22.03.2015 15:21:03
 *  Author: Christian Möllers
 *
 * First release
 * 
 *
 *
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../CMGlobal.h"

#include <util/delay.h>

#include "CMHh10d.h"
#include "../CMTwi.h"

#define hh10dAddressRead	0xA2
#define hh10dEEpromAddress	0x0A
//#define hh10EEpromAddress	0x09
#define hh10dAddressWrite	0xA3

#define twWrite 0
#define twRead 1

int get_freq( void )
{
	cli();
	TCCR1B = 0x00;
	TCCR1A = 0x00;
	TCCR1B = ( 1 << CS12 ) | ( 1 << CS11 ) | ( 1 << CS10 );
	DDRD &=~ ( 1 << PD5 );
	sei();
	int r_freq = 0;
	TCNT1 = 0;
	_delay_ms(100);				// 100 ms messen
	r_freq = TCNT1;
	r_freq = r_freq * 10;
	return( r_freq );			// Ergebnis * 10 nehmen
}

float readhumiditySensor(void)
{
	int msb = 0;
	int lsb = 0;
	int sens = 0;			// sensitivity One-Byte-Value
	int offset = 0;			// offset Two-Byte-Value
	float rh = 0.0;
	
	i2cInit();
	i2cStart(hh10dAddressRead);
	i2c_write(hh10dEEpromAddress);	
	i2cStop();
	i2cStart(hh10dAddressWrite);
	
	msb = i2cReceiveAck();
	lsb = i2cReceiveAck();
	
	sens = msb * 256;			// Stellenwert von sens-MSB berücksichten
	sens = sens + lsb;		// MSB und LSB von sens zusammenfügen
	
	msb = i2cReceiveAck();
	lsb = i2cReceiveNack();
	
	offset = msb * 256;		// Stellenwert von offset-MSB berücksichten
	offset = offset + lsb;	// MSB und LSB von Offset zusammenfügen
	
	i2cStop();
	rh = ( offset - get_freq() );
	rh = rh * sens;
	rh = rh / 4096;
	
	return rh;
}